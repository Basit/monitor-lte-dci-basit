#!/bin/bash

timeout -s SIGINT "$1" /local/tools/falcon/build/src/FalconEye -D "$2"/dci.csv -E "$2"/stat.csv -r -N -f "$3" -W "$4" 2>&1 | tee "$2"/log.txt


