#!/bin/bash
#
# Install the RF monitor and its dependencies.
#
wget -O - http://repos.emulab.net/emulab.key | sudo apt-key add -
if [ $? -ne 0 ]; then
    echo 'apt-key add failed'
    exit 1
fi

RELEASE="$(. /etc/os-release ; echo $UBUNTU_CODENAME)"

REPO="powder"

# WTF! The tabs before priority actually matter.
echo "http://boss/mirror/repos.emulab.net/$REPO/ubuntu	priority:1" | sudo tee -a /etc/apt/emulab-$REPO-mirrorlist.txt &&
    echo "http://repos.emulab.net/$REPO/ubuntu	priority:2" | sudo tee -a /etc/apt/emulab-$REPO-mirrorlist.txt &&
    echo "deb mirror+file:/etc/apt/emulab-$REPO-mirrorlist.txt $RELEASE main" | sudo tee -a /etc/apt/sources.list.d/$REPO.list
if [ $? -ne 0 ]; then
    echo "creating $REPO failed"
    exit 1
fi

REPO="powder-endpoints"

# WTF! The tabs before priority actually matter.
echo "http://boss/mirror/repos.emulab.net/$REPO/ubuntu	priority:1" | sudo tee -a /etc/apt/emulab-$REPO-mirrorlist.txt &&
    echo "http://repos.emulab.net/$REPO/ubuntu	priority:2" | sudo tee -a /etc/apt/emulab-$REPO-mirrorlist.txt &&
    echo "deb mirror+file:/etc/apt/emulab-$REPO-mirrorlist.txt $RELEASE main" | sudo tee -a /etc/apt/sources.list.d/$REPO.list
if [ $? -ne 0 ]; then
    echo "creating $REPO failed"
    exit 1
fi

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get -y install --no-install-recommends python3-tk socat tigervnc-standalone-server autocutsel fvwm
if [ $? -ne 0 ]; then
    echo 'apt-get install support failed'
    exit 1
fi

bash /local/repository/install-falcon.sh
if [ $? -ne 0 ]; then
    echo 'Falcon install failed'
    exit 1
fi



# Ick
sudo sed -i.bak -e 's/join(req_args, ",")/join(",", req_args)/' /usr/local/bin/script_wrapper.py

# Performance setup for Falcon
host=$( cat /proc/sys/kernel/hostname )
if [ "$host" != *"-host"* ]; then
    gov="performance"
    echo "Setting CPU governor to '$gov'"
    for file in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do echo "$gov" | sudo tee $file; done
fi

prog="/local/tools/falcon/build/src/FalconEye"
cmd="sudo setcap cap_sys_nice=eip $prog"
$cmd


#
# Marker that says we completed the install. In case we have to power
# cycle to bring the B210 back to life.
#
sudo mkdir -p /etc/monitor

sudo mkdir /mydata/falconrun


sudo ntpdate -u ops.emulab.net && sudo ntpdate -u ops.emulab.net

sudo touch /etc/monitor/.ready

exit 0
